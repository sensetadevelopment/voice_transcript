"""
Send each audio file in a directory structure to VoiceBase for transcription. This program requires the Python library
requests to be installed.
"""

import os
import os.path
import logging
import json

import requests


audio_exts = set(['.mp3', '.wav'])

api = 'https://api.voicebase.com/services'
transcript_type = 'machine'


def send_file(file_path, api_key_, api_password_, lang_code_, results_file_):
    """
    Send a file to VoiceBase for transcription.

    :param file_path: file path of audio file to upload.
    :param api_key_: API key provided by VoiceBase.
    :param api_password_: API password of VoiceBase.
    :param lang_code_: language code to use for transcription.
    :param results_file_: file to store resulting metadata for each sent file.
    :return:
    """
    params = {'version': '1.1', 'apikey': api_key_, 'password': api_password_, 'action': 'uploadMedia',
              'lang': lang_code_, 'transcriptType': transcript_type}
    file_dir, file_name = os.path.split(file_path)
    file_obj = open(file_path, 'rb')
    file_data = file_obj.read()
    files_params = {'file': (file_name, file_data, 'audio/mp3')}
    res = requests.post(api, params, files=files_params)
    logging.info('Sent file: ' + str(file_path))
    content = json.loads(res.content)
    if content['requestStatus'] != 'SUCCESS':
        logging.error('Error uploading file' + str(file_path) + ' statusMessage: ' + content['statusMessage'])
    media_id = content['mediaId']
    file_url = content['fileUrl']
    upload_data = {'fileName': file_name, 'filePath': file_path, 'mediaId': media_id, 'fileUrl': file_url}
    out_file = open(results_file_, 'a')
    out_file.write(json.dumps(upload_data) + '\n')
    out_file.close()


def process_tree(source_dir_, api_key_, api_password_, lang_code_, results_file_):
    """
    Process a tree of files to upload to VoiceBase.

    :param source_dir_: source directory with (subdirectories and) audio files.
    :param api_key_: API key of VoiceBase.
    :param api_password_: API password of VoiceBase.
    :param lang_code_: language code to use for machine transcription.
    :param results_file_: file to store the resulting metadata for each uploaded file.
    :return:
    """
    for dir_path, dir_names, file_names in os.walk(source_dir_):
        for file_name in file_names:
            file_only_name, file_ext = os.path.splitext(file_name)
            if file_ext not in audio_exts:
                continue
            process_file_name = file_only_name
            file_path = os.path.join(dir_path, process_file_name + file_ext)
            send_file(file_path, api_key_, api_password_, lang_code_, results_file_)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('source_dir', help='the source directory with mp3 files.')
    parser.add_argument('api_key', help='API key for VoiceBase.')
    parser.add_argument('api_password', help='API password for VoceBase.')
    parser.add_argument('--lang_code', help='language code to give to the API.', default='es-MEX')
    parser.add_argument('--results_file', help='results file to store information about sent files.',
                        default='data.txt')
    args = parser.parse_args()
    source_dir = args.source_dir
    api_key = args.api_key
    api_password = args.api_password
    lang_code = args.lang_code
    results_file = args.results_file

    process_tree(source_dir, api_key, api_password, lang_code, results_file)