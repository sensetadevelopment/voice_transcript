"""
Process a directory with (subdirectories and) audio files. For each file, generate two files with left and right
channels and write them to a destiny directory. This program requires that FFMPEG is installed and located using the
PATH variable.
"""

import os
import os.path
import subprocess
import logging

audio_exts = set(['.mp3', '.wav'])


def convert_audio_file(in_file_path, left_out_file_path, right_out_file_path):
    """
    Convert an audio file with two channels to two files, one for each channel.

    :param in_file_path: file path of original file with two channels.
    :param left_out_file_path: file path of output file for left channel.
    :param right_out_file_path: file path of output file for right channel.
    :return:
    """
    split_arg = ['ffmpeg',
                 '-i',
                 in_file_path,
                 '-map_channel',
                 '0.0.0',
                 left_out_file_path,
                 '-map_channel',
                 '0.0.1',
                 right_out_file_path]
    print ' '.join(split_arg)
    subprocess.call(split_arg)


def process_tree(source_dir_, destiny_dir_, output_ext_):
    """
    Process a source directory with files. For each file inside, if it has a supported file extension, split it in two
    files and store them in the destiny directory. Save the files with the output extension.

    :param source_dir_: source directory with files to process.
    :param destiny_dir_: destiny directory in where to store processed files.
    :param output_ext_: output extension to use for output files.
    :return:
    """
    out_ext = output_ext_
    for dir_path, dir_names, file_names in os.walk(source_dir_):
        for file_name in file_names:
            file_only_name, file_ext = os.path.splitext(file_name)
            if file_ext not in audio_exts:
                continue
            out_dir_path = dir_path.replace(source_dir_, destiny_dir_)
            if not os.path.isdir(out_dir_path):
                os.makedirs(out_dir_path)
            process_file_name = file_only_name
            in_file_path = os.path.join(dir_path, process_file_name + file_ext)
            left_out_file_path = os.path.join(out_dir_path, process_file_name + '_l' + out_ext)
            right_out_file_path = os.path.join(out_dir_path, process_file_name + '_r' + out_ext)
            logging.info('Processing file: ' + str(in_file_path))
            convert_audio_file(in_file_path, left_out_file_path, right_out_file_path)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('source_dir', help='the source directory with mp3 files.')
    parser.add_argument('destiny_dir', help='the destiny directory in where to output wav files.')
    parser.add_argument('--output-ext', help='output extension to use.', default='.mp3')
    args = parser.parse_args()
    destiny_dir = args.destiny_dir
    source_dir = args.source_dir
    output_ext = args.output_ext

    process_tree(source_dir, destiny_dir, output_ext)