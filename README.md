Transcript with VoiceBase
=========================

These scripts help generating transcriptions of audio files using VoiceBase.

Requirements
------------

You need to create an account in: <https://app.voicebase.com>

They send you an email with a "password" for that account, you will need that password.

You need to generate an API key, send an email asking it to: <support@voicebase.com>

Environment requirements
------------------------

You need to have installed FFMPEG: <https://www.ffmpeg.org/>
And it should be accessible in the PATH, you should be able to type in the command line:

```
ffmpeg -version
```

and get something like:

```
ffmpeg version N-50911-g9efcfbe
built on Mar 13 2013 21:26:48 with gcc 4.7.2 (GCC)
configuration: --enable-gpl --enable-version3 --disable-w32threads --enable-avis
ynth --enable-bzlib --enable-fontconfig --enable-frei0r --enable-gnutls --enable
-libass --enable-libbluray --enable-libcaca --enable-libfreetype --enable-libgsm
 --enable-libilbc --enable-libmp3lame --enable-libopencore-amrnb --enable-libope
ncore-amrwb --enable-libopenjpeg --enable-libopus --enable-librtmp --enable-libs
chroedinger --enable-libsoxr --enable-libspeex --enable-libtheora --enable-libtw
olame --enable-libvo-aacenc --enable-libvo-amrwbenc --enable-libvorbis --enable-
libvpx --enable-libx264 --enable-libxavs --enable-libxvid --enable-zlib
libavutil      52. 19.100 / 52. 19.100
libavcodec     55.  0.100 / 55.  0.100
libavformat    55.  0.100 / 55.  0.100
libavdevice    54.  4.100 / 54.  4.100
libavfilter     3. 45.103 /  3. 45.103
libswscale      2.  2.100 /  2.  2.100
libswresample   0. 17.102 /  0. 17.102
libpostproc    52.  2.100 / 52.  2.100
```

You need to have installed the `requests` Python library.
 
You can install it with:

```
pip install requests
```

Usage
-----

**Splitting channels**

To split all audio files in a directory having 2 audio channels, with different information in each channel, and output
the resutls to different audio files in another directory, run:
 
 ```
 python split_audio.py source_dir destiny_dir
 ```
 
 For example:
 
 ```
 python split_audio.py /home/user/audio_in/ /home/user/audio_out/
 ```
 
 You can get more information with:
 
 ```
 python split_audio.py -h
 ```
 
 **Sending files**
 
 To send all the audio files in a directory to be processed by VoiceBase, run:
 
 ```
 python send_audio.py source_dir api_key api_password
 ```
 
 You need to use your own API key and password.
 
 For example:
 
 ```
 python send_audio.py /home/user/audio_out/ 1234-56785678-ABCDF1234-AB123 les8sdfl23
 ```
 
  
 It will generate a text file with metadata of each uploaded file to VoiceBase in your current directory. A file like:
 
```
data.txt
```
 
 You can get more information with:
 
 ```
 python send_audio.py -h
 ```
 
 You can go to the VoiceBase web interface to check the processing files.
 
 **Getting transcriptions**
 
 Once the files have finished processing, you can download the transcriptions with:
 
 ```
 python get_transcript.py source_data api_key api_password
 ```
 
 You need to use your own API key and password.
 
 You should use the text file generated with `send_audio.py` as the `source_data` parameter. For example:
 
 ```
 python get_transcript.py data.txt 1234-56785678-ABCDF1234-AB123 les8sdfl23
 ```
 
 It will download a text file with the transcript for each audio file uploaded and it will store that file with the 
 same name and in the same place as the original uploaded file but with a text (.txt, .json) extension. 
 
 You can get more information with:
 
 ```
 python get_transcriptions.py -h
 ```